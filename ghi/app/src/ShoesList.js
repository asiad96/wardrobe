import React, { useState, useEffect } from "react";


function ShoesList(props) {
    const [shoes, setShoes] = useState([])

    const handleDelete = async (event, id) => {
        event.preventDefault()

        const fetchOptions = {
            headers: {
                "Content-Type": "application/json",
            },
            method: "DELETE"
        }

        try {
            const response = await fetch(`http://localhost:8080/api/shoes/${id}/`, fetchOptions);
            if (response.ok) {
                const data = await response.json();
                loadShoes()

            }
        } catch (error) {
            console.error(error)
        }
    }

    async function loadShoes() {
        try {
            const response = await fetch('http://localhost:8080/api/shoes/');
            if (response.ok) {
                const data = await response.json();
                setShoes(data.shoes)
            } else {
                console.error(response);
            }
        } catch (e) {
            console.error("No shoes found", e)
        }
    }

    useEffect(() => {
        loadShoes()
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>color</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {shoes.map((shoe) => (
                    <tr key={shoe.id}>
                        <td>{shoe.model_name}</td>
                        <td>{shoe.manufacturer}</td>
                        <td>{shoe.color}</td>
                        <td> <img src={shoe.picture_url} alt={shoe.picture_url} width='150' roundedCircle /> </td>
                        <td><a onClick={(event) => handleDelete(event, shoe.id)} type="button" className="btn btn-link">Delete</a></td>
                    </tr>
                ))}
            </tbody>
        </table>
    );

}

export default ShoesList;
