import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

function HatList(props) {
  const [hats, setHats] = useState([])

  const handleDeleteHat = async (event, id) => {
      event.preventDefault()

      const fetchOptions = {
          headers: {
              "Content-Type": "application/json",
          },
          method: "DELETE"
      }

      try {
          const response = await fetch(`http://localhost:8090/api/hats/${id}/`, fetchOptions);
          if (response.ok) {
              const data = await response.json();
              loadHats()

          }
      } catch (error) {
          console.error(error)
      }
  }

  async function loadHats() {
    try {
        const response = await fetch('http://localhost:8090/api/hats/');
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
        } else {
            console.error(response);
        }
    } catch (e) {
        console.error("No hats found", e)
    }
}
loadHats()

  useEffect(() => {
      loadHats()
  }, [])


    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Style</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
              {hats.map((hat) => (
                <tr key={hat.id}>
                  <td>{hat.style}</td>
                  <td>{hat.fabric}</td>
                  <td>{hat.color}</td>
                  <td>
                    <img src={hat.picture} alt={hat.style} style={{ width: '100px' }} />
                  </td>
                  <td>
                    <a
                      onClick={(event) => handleDeleteHat(event, hat.id)}
                      type="button"
                      className="btn btn-link"
                    >
                      Delete
                    </a>
                  </td>
                </tr>
              ))}
            </tbody>
        </table>
    );

}

export default HatList;
